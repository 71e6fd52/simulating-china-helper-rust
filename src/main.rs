#[macro_use]
extern crate failure;

use std::result;

use chrono::{prelude::*, FixedOffset, TimeZone};
use failure::Fail;
use http::{header, Request, Response, StatusCode};
use lazy_static::lazy_static;
use now_lambda::{error::NowError, lambda};
use rand::{
    distributions::{Distribution, Normal},
    prelude::*,
};
use regex::{Captures, Regex};
use serde_json::{json, Value};
use telegram_bot_fork::*;

pub use self::network::Api as TelegramApi;
use self::{attributes::Player, constant::Tokens, error::Error};

pub mod airtable;
pub mod attributes;
pub mod constant;
pub mod error;
pub mod network;

type Result<T> = std::result::Result<T, failure::Error>;

#[inline]
fn cst() -> FixedOffset {
    FixedOffset::east(8 * 3600)
}

#[inline]
fn start_real() -> Date<FixedOffset> {
    cst().ymd(2019, 3, 14)
}

#[inline]
fn start_sim() -> Date<FixedOffset> {
    cst().ymd(2018, 12, 3)
}

fn date_r2s(real: Date<FixedOffset>) -> Date<FixedOffset> {
    start_sim() + (real - start_real())
}

fn date_s2r(sim: Date<FixedOffset>) -> Date<FixedOffset> {
    start_real() + (sim - start_sim())
}

fn time_r2s(real: DateTime<FixedOffset>) -> DateTime<FixedOffset> {
    let time = real.time();
    (start_sim() + (real.date() - start_real()))
        .and_time(time)
        .expect("invalid datetime")
}

fn time_s2r(sim: DateTime<FixedOffset>) -> DateTime<FixedOffset> {
    let time = sim.time();
    (start_real() + (sim.date() - start_sim()))
        .and_time(time)
        .expect("invalid datetime")
}

fn parse_dice(args: &[&str]) -> Result<(i32, i32)> {
    match args.len() {
        0 => Ok((1, 6)),
        1 => {
            lazy_static! {
                static ref RE: Regex = Regex::new(r"^(\d+)d(\d+)$").expect("regex failed");
            }
            let s = RE.captures(args[0]).ok_or(Error::Argument)?;
            Ok((s[1].parse()?, s[2].parse()?))
        }
        2 => Ok((args[0].parse()?, args[1].parse()?)),
        _ => Err(Error::Argument.into()),
    }
}

#[derive(Debug, PartialEq, Eq)]
enum ConvertDirection {
    Sim2Real,
    Real2Sim,
}

pub struct Parser<'a> {
    message: &'a Message,
    text: &'a str,
    matches: Captures<'a>,
    tokens: &'a Tokens,
    api: &'a TelegramApi,
}

impl<'a> Parser<'a> {
    pub fn new(
        message: &'a Message,
        tokens: &'a Tokens,
        api: &'a TelegramApi,
    ) -> Option<Parser<'a>> {
        lazy_static! {
            static ref RE: Regex =
                Regex::new(r"^/(\S+?)(?:@SimulatingChinaBot)?(?s:\s(.+))?$").expect("regex failed");
        }
        let text = if let MessageKind::Text { data: text, .. } = &message.kind {
            text
        } else {
            return None;
        };
        let matches = RE.captures(text)?;
        Some(Parser {
            message,
            text,
            matches,
            tokens,
            api,
        })
    }

    #[inline]
    pub fn command(&self) -> &str {
        &self.matches[1]
    }

    pub fn args(&self) -> Vec<&str> {
        if self.matches.get(2).is_none() {
            vec![]
        } else {
            self.matches[2].split_whitespace().collect::<Vec<&str>>()
        }
    }

    #[inline]
    pub fn username(&self) -> Result<&str> {
        match &self.message.from.username {
            Some(username) => Ok(username),
            None => Err(Error::NoUsername.into()),
        }
    }

    pub fn args_text(&self) -> Result<&str> {
        let message = self.text;
        let index = message.find(char::is_whitespace).ok_or(Error::Argument)?;
        let (_, message) = message.split_at(index + 1);
        Ok(message)
    }

    pub fn send(&self, message: &str) -> Response<String> {
        let res = json!({
            "method": "sendMessage",
            "chat_id": self.message.chat.id().0,
            "text": message,
        });
        res_json(&res)
    }

    pub fn reply(&self, message: &str) -> Response<String> {
        let res = json!({
            "method": "sendMessage",
            "chat_id": self.message.chat.id().0,
            "text": message,
            "reply_to_message_id": self.message.id.0,
        });
        res_json(&res)
    }

    fn start(&self) -> Response<String> {
        self.send("II主群：https://t.me/joinchat/FP3nTxbZjtluIrIWwnl1xg")
    }

    fn help(&self) -> Response<String> {
        self.send(
            r"today - current simulating date
now - current simulating time
help - get command list
echo <message> - repeat message
chat_id - get current chat id
real2sim <year> <month> <day> - convert a real time to simulating time
sim2real <year> <month> <day> - convert a simulating time to real time
source - get source of this bot
normal <mean> <standard deviation> - get a random number by normal distribution
dice - parse dice
random - get a random number in 0..1
me - get self information
suicide - suicide
plot_member - Get the current list of plot committee members
to_plot <message> - Send message to plot committee",
        )
    }

    fn source(&self) -> Response<String> {
        self.reply("https://gitlab.com/71e6fd52/simulating-china-helper-rust")
    }

    fn echo(&self) -> Result<Response<String>> {
        let message = self.args_text()?;
        Ok(if message.starts_with("sc") {
            self.send(&format!("-{}", message))
        } else {
            self.send(message)
        })
    }

    fn chat_id(&self) -> Response<String> {
        self.send(&self.message.chat.id().0.to_string())
    }

    fn today(&self) -> Response<String> {
        let time = time_r2s(Utc::now().with_timezone(&cst()));
        self.send(&time.format("今天是 %Y 年 %m 月 %d 日").to_string())
    }

    fn now(&self) -> Response<String> {
        let time = time_r2s(Utc::now().with_timezone(&cst()));
        self.send(
            &time
                .format("今天是 %Y 年 %m 月 %d 日 %H:%M:%S")
                .to_string(),
        )
    }

    fn random(&self) -> Response<String> {
        self.reply(&format!("{}", random::<f64>()))
    }

    fn normal(&self) -> Result<Response<String>> {
        if self.args().len() != 2 {
            return Err(Error::Argument.into());
        }
        let a = self
            .args()
            .iter()
            .map(|n| n.parse())
            .collect::<std::result::Result<Vec<f64>, std::num::ParseFloatError>>()?;
        let normal = Normal::new(a[0], a[1]);
        let v = normal.sample(&mut rand::thread_rng());
        Ok(self.reply(&format!("{}", v)))
    }

    fn dice(&self) -> Result<Response<String>> {
        let result = parse_dice(&self.args())?;
        let mut rng = thread_rng();
        let sum = (0..result.0).fold(0, |s, _| s + rng.gen_range(1, result.1 + 1));
        Ok(self.reply(&format!("{}", sum)))
    }

    fn convert(&self, dir: ConvertDirection) -> Result<Response<String>> {
        if self.args().len() != 3 {
            return Err(Error::Argument.into());
        }
        let a = self
            .args()
            .iter()
            .map(|n| n.parse())
            .collect::<std::result::Result<Vec<u16>, std::num::ParseIntError>>()?;
        let time = cst().ymd(a[0].into(), a[1].into(), a[2].into());
        let time = if dir == ConvertDirection::Real2Sim {
            date_r2s(time)
        } else {
            date_s2r(time)
        };
        Ok(self.reply(&time.format("这是 %Y 年 %m 月 %d 日").to_string()))
    }

    fn me(&self) -> Result<Response<String>> {
        if let MessageChat::Private(_) = self.message.chat {
        } else {
            return Ok(self.reply("This command can only use in private message."));
        }
        let api = airtable::Client::new(&self.tokens.airtable);
        let table = airtable::Table::new("apprCGBPfpxabdGps", "玩家");

        let username = self.username()?;

        let player = api.list_or_create_fn(
            &table,
            &format!("{} = \"{}\"", "{username}", username),
            || Player::random(&mut rand::thread_rng(), username.to_string()),
        )?;
        Ok(self.reply(&player[0].fields.format_player_attr()))
    }

    fn suicide(&self) -> Result<Response<String>> {
        if let MessageChat::Private(_) = self.message.chat {
        } else {
            return Ok(self.reply("This command can only use in private message."));
        }
        let api = airtable::Client::new(&self.tokens.airtable);
        let table = airtable::Table::new("apprCGBPfpxabdGps", "玩家");

        let player = Player {
            username: self.username()?.to_string(),
            administrative: -1.0,
            speech: -1.0,
            social: -1.0,
        };
        api.update_or_create(
            &table,
            &format!("{} = \"{}\"", "{username}", self.username()?),
            &player,
        )?;
        Ok(self.reply(&player.format_player_attr()))
    }

    fn plot_member(&self) -> Result<Response<String>> {
        Ok(self.reply(
            &self
                .api
                .send(GetChatAdministrators::new(ChatId::new(constant::PLOT_ID)))?
                .into_iter()
                .map(|m| m.user.username.ok_or(Error::NoUsername))
                .collect::<std::result::Result<Vec<String>, Error>>()?
                .into_iter()
                .map(|u| "@".to_string() + &u)
                .collect::<Vec<String>>()
                .join(" "),
        ))
    }

    fn to_plot(&self) -> Result<Response<String>> {
        let text = format!(
            "scpm {} {}\n---\n@{}: {}",
            self.message.chat.id(),
            self.message.id,
            self.username()?,
            self.args_text()?
        );
        let res = json!({
            "method": "sendMessage",
            "chat_id": constant::PLOT_ID,
            "text": text,
        });
        Ok(res_json(&res))
    }

    pub fn call(&self) -> Result<Response<String>> {
        Ok(match self.command() {
            "start" => self.start(),
            "help" => self.help(),
            "echo" => self.echo()?,
            "source" => self.source(),
            "chat_id" => self.chat_id(),
            "today" => self.today(),
            "now" => self.now(),
            "random" => self.random(),
            "normal" => self.normal()?,
            "dice" => self.dice()?,
            "me" => self.me()?,
            "suicide" => self.suicide()?,
            "plot_member" => self.plot_member()?,
            "to_plot" => self.to_plot()?,
            "real2sim" => self.convert(ConvertDirection::Real2Sim)?,
            "sim2real" => self.convert(ConvertDirection::Sim2Real)?,
            _ => empty(),
        })
    }
}

fn pass_message(message: &Message) -> Option<Response<String>> {
    let reply_message = if let MessageOrChannelPost::Message(reply_message) =
        message.reply_to_message.as_ref()?.as_ref()
    {
        reply_message
    } else {
        return None;
    };
    let reply_text = if let MessageKind::Text { data: text, .. } = &reply_message.kind {
        text
    } else {
        return None;
    };
    println!("|-- Reply: {}", reply_text);
    let text = if let MessageKind::Text { data: text, .. } = &message.kind {
        text
    } else {
        return None;
    };
    let mut iter = reply_text.split_whitespace();
    if iter.next() != Some("scpm") {
        return None;
    }
    println!("|-- scpm");
    let chat = ChatId::new(iter.next()?.parse().ok()?);
    println!("|  |-- chat: {}", chat);
    let reply_id = MessageId::new(iter.next()?.parse().ok()?);
    println!("|  |-- reply_id: {}", reply_id);
    let send = format!(
        "scpm {} {}\n---\n@{}: {}",
        message.chat.id(),
        message.id,
        message.from.username.as_ref()?,
        text
    );
    let res = json!({
        "method": "sendMessage",
        "chat_id": chat,
        "text": send,
        "reply_to_message_id": reply_id,
    });
    Some(res_json(&res))
}

fn parse(data: Update) -> Response<String> {
    let message = match data.kind {
        UpdateKind::Message(message) => message,
        UpdateKind::EditedMessage(message) => message,
        _ => return empty(),
    };
    if let MessageKind::Text { data: text, .. } = &message.kind {
        println!("Receive message: {}", text);
    } else {
        return empty();
    };
    if let Some(res) = pass_message(&message) {
        return res;
    }
    let tokens = Tokens::new().unwrap();
    let api = TelegramApi::new(&tokens.telegram);
    let parser = if let Some(parser) = Parser::new(&message, &tokens, &api) {
        parser
    } else {
        return empty();
    };
    println!(
        "command: {}, args: {}",
        parser.command(),
        parser.args().join(", ")
    );
    parser.call().unwrap_or_else(|err| {
        let mut s = "Error: ".to_string();
        s += &err.to_string();
        parser.reply(&s)
    })
}

fn handler(request: Request<Vec<u8>>) -> result::Result<Response<String>, NowError> {
    let v: Update = serde_json::from_slice(request.body()).expect("json parse fail");

    Ok(parse(v))
}

fn empty() -> Response<String> {
    Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, "application/json")
        .body("{}".to_string())
        .expect("failed to render response")
}

fn res_json(json: &Value) -> Response<String> {
    Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, "application/json")
        .body(json.to_string())
        .expect("failed to render response")
}

fn main() {
    lambda!(handler);
}
