#[derive(Fail, Debug)]
pub enum Error {
    #[fail(display = "wrong number of arguments")]
    Argument,
    #[fail(display = "no username")]
    NoUsername,
}

impl Into<now_lambda::error::NowError> for Error {
    fn into(self) -> now_lambda::error::NowError {
        now_lambda::error::NowError::new(&format!("{}", self))
    }
}
