use reqwest::{header::CONTENT_TYPE, Client, Method, Url};
use telegram_bot_fork::{Request, ResponseType};
use telegram_bot_fork_raw::{
    requests::_base::{Body as TelegramBody, Method as TelegramMethod},
    HttpResponse,
};

#[derive(Clone)]
pub struct Api {
    uri: Option<String>,
    client: Client,
    token: String,
}

impl Api {
    pub fn new<T: AsRef<str>>(token: T) -> Self {
        Api {
            uri: None,
            client: Client::new(),
            token: token.as_ref().to_string(),
        }
    }

    pub fn set_uri<T: AsRef<str>>(&mut self, uri: T) -> &mut Self {
        self.uri = Some(uri.as_ref().into());

        self
    }

    pub fn send<Req: Request>(
        &self,
        request: Req,
    ) -> Result<<Req::Response as ResponseType>::Type, failure::Error> {
        let request = request
            .serialize()
            .map_err(|err| format_err!("{:?}", err.kind()))?;

        let uri = request
            .url
            .url(self.uri.as_ref().map(String::as_str), &self.token);

        let method = match request.method {
            TelegramMethod::Get => Method::GET,
            TelegramMethod::Post => Method::POST,
        };
        let mut http_request = reqwest::Request::new(method, Url::parse(&uri)?);

        match request.body {
            TelegramBody::Empty => {
                println!("Send Body is empty");
            }
            TelegramBody::Json(body) => {
                println!("Send Body {}", String::from_utf8_lossy(&body));
                *http_request.body_mut() = Some(body.into());
                http_request
                    .headers_mut()
                    .insert(CONTENT_TYPE, "application/json".parse().unwrap());
            }
            body => panic!("Unknown body type {:?}", body),
        };

        let mut body: Vec<u8> = vec![];

        self.client.execute(http_request)?.copy_to(&mut body)?;

        let response = HttpResponse { body: Some(body) };

        Req::Response::deserialize(response).map_err(|err| format_err!("{:?}", err.kind()))
    }
}
