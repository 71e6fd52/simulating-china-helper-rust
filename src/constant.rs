use super::Result;
use std::env;

#[allow(clippy::unreadable_literal)]
pub const PLOT_ID: i64 = -1001428866082;

pub struct Tokens {
    pub telegram: String,
    pub airtable: String,
}

impl Tokens {
    pub fn new() -> Result<Tokens> {
        Ok(Tokens {
            telegram: env::var("TELEGRAM_BOT_TOKEN")?,
            airtable: env::var("AIRTABLE_API_KEY")?,
        })
    }
}
