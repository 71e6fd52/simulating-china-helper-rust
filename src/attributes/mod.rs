use serde_derive::*;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Player {
    pub username: String,
    #[serde(rename = "行政")]
    pub administrative: f64,
    #[serde(rename = "演说")]
    pub speech: f64,
    #[serde(rename = "社交")]
    pub social: f64,
}

impl Player {
    pub fn random<T>(rng: &mut T, username: String) -> Player
    where
        T: rand::Rng,
    {
        Player {
            username,
            administrative: rng.gen(),
            speech: rng.gen(),
            social: rng.gen(),
        }
    }

    pub fn al2text(f: f64) -> &'static str {
        match f {
            i if i < 0.0 => "错误",
            i if i < 0.1 => "极低",
            i if i < 0.4 => "低",
            i if i < 0.6 => "中",
            i if i < 0.9 => "高",
            i if i < 1.0 => "极高",
            _ => "错误",
        }
    }

    pub fn format_player_attr(&self) -> String {
        if self.administrative < 0.0 {
            "已死亡".to_string()
        } else {
            format!(
                r"行政: {}
演说: {}
社交: {}",
                Self::al2text(self.administrative),
                Self::al2text(self.speech),
                Self::al2text(self.social),
            )
        }
    }
}
