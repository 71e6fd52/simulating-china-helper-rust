use reqwest::Url;
use serde_derive::*;
use url::percent_encoding::{utf8_percent_encode, DEFAULT_ENCODE_SET};

use crate::attributes::Player;

use super::Result;

pub fn encode(s: &str) -> String {
    utf8_percent_encode(s, DEFAULT_ENCODE_SET).to_string()
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
enum Response<T> {
    Ok(T),
    Error(Error),
}

impl<T> Into<Option<T>> for Response<T> {
    fn into(self) -> Option<T> {
        match self {
            Response::Ok(v) => Some(v),
            Response::Error(_) => None,
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct Error {
    r#type: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct Records<T> {
    records: Vec<Record<T>>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Record<T> {
    pub id: String,
    pub fields: T,
    pub created_time: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Fields<T> {
    pub fields: T,
}

impl<T> Fields<T> {
    pub fn new(fields: T) -> Fields<T> {
        Fields { fields }
    }
}

pub struct Client<'a> {
    http: reqwest::Client,
    token: &'a str,
}

impl<'a> Client<'a> {
    pub fn new(token: &'a str) -> Client<'a> {
        Client {
            token,
            http: reqwest::Client::new(),
        }
    }

    pub fn list_all(&self, table: &Table) -> Result<Vec<Record<Player>>> {
        self.list(table, "true")
    }

    pub fn list(&self, table: &Table, formula: &str) -> Result<Vec<Record<Player>>> {
        let mut uri = Url::parse(&table.uri())?;
        uri.query_pairs_mut()
            .append_pair("filterByFormula", formula);
        Ok(self
            .http
            .get(uri)
            .bearer_auth(self.token)
            .send()?
            .json::<Records<Player>>()?
            .records)
    }

    pub fn get(&self, table: &Table, id: &str) -> Result<Option<Record<Player>>> {
        Ok(self
            .http
            .get(&format!("{}/{}", table.uri(), id))
            .bearer_auth(self.token)
            .send()?
            .json::<Response<Record<Player>>>()?
            .into())
    }

    pub fn create(&self, table: &Table, value: &Player) -> Result<Record<Player>> {
        Ok(self
            .http
            .post(&table.uri())
            .bearer_auth(self.token)
            .json(&Fields::new(value))
            .send()?
            .json()?)
    }

    pub fn update(&self, table: &Table, id: &str, value: &Player) -> Result<Record<Player>> {
        Ok(self
            .http
            .patch(&format!("{}/{}", table.uri(), id))
            .bearer_auth(self.token)
            .json(&Fields::new(value))
            .send()?
            .json()?)
    }

    pub fn update_fn<T>(&self, table: &Table, id: &str, func: T) -> Result<Record<Player>>
    where
        T: FnOnce(Option<Record<Player>>) -> Player,
    {
        self.update(table, id, &func(self.get(table, id)?))
    }

    pub fn update_or_create(
        &self,
        table: &Table,
        formula: &str,
        value: &Player,
    ) -> Result<Vec<Record<Player>>> {
        let result = self.list(table, formula)?;
        if result.is_empty() {
            Ok(vec![self.create(table, value)?])
        } else {
            result
                .into_iter()
                .map(|record| self.update(table, &record.id, value))
                .collect()
        }
    }

    pub fn update_or_create_fn<T>(
        &self,
        table: &Table,
        formula: &str,
        func: T,
    ) -> Result<Vec<Record<Player>>>
    where
        T: Fn(Option<Player>) -> Player,
    {
        let result = self.list(table, formula)?;
        if result.is_empty() {
            Ok(vec![self.create(table, &func(None))?])
        } else {
            result
                .into_iter()
                .map(|record| self.update(table, &record.id, &func(Some(record.fields))))
                .collect()
        }
    }

    pub fn list_or_create(
        &self,
        table: &Table,
        formula: &str,
        value: &Player,
    ) -> Result<Vec<Record<Player>>> {
        let result = self.list(table, formula)?;
        Ok(if result.is_empty() {
            vec![self.create(table, value)?]
        } else {
            result
        })
    }

    pub fn list_or_create_fn<T>(
        &self,
        table: &Table,
        formula: &str,
        func: T,
    ) -> Result<Vec<Record<Player>>>
    where
        T: FnOnce() -> Player,
    {
        let result = self.list(table, formula)?;
        Ok(if result.is_empty() {
            vec![self.create(table, &func())?]
        } else {
            result
        })
    }
}

pub struct Table {
    base: String,
    table: String,
}

impl Table {
    const URI: &'static str = "https://api.airtable.com/v0";

    pub fn new(base: &str, table: &str) -> Table {
        let base = base.to_string();
        let table = encode(table);
        Table { base, table }
    }

    pub fn uri(&self) -> String {
        format!("{}/{}/{}", Self::URI, self.base, self.table)
    }
}
