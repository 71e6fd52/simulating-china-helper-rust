today - current simulating date
now - current simulating time
help - get command list
echo - repeat message
chat_id - get current chat id
real2sim - convert a real time to simulating time
sim2real - convert a simulating time to real time
source - get source of this bot
normal - get a random number by normal distribution
dice - parse dice
random - get a random number in 0..1
me - get self information
suicide - suicide
plot_member - get the current list of plot committee members
to_plot - Send message to plot committee
